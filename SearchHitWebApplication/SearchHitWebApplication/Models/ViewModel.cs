﻿using System.ComponentModel.DataAnnotations;

namespace SearchHitWebApplication.Models {
    public class ViewModel {
        public string SearchQuery { get; set; }

        public string GoogleSearchHits { get; set; }

        public string BingSearchHits { get; set; }
    }
}