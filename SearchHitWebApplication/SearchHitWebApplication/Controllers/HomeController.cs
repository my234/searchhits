﻿using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Google.Apis.Services;
using HtmlAgilityPack;
using Microsoft.Ajax.Utilities;
using SearchHitWebApplication.Models;

namespace SearchHitWebApplication.Controllers {
    public class HomeController : Controller {

        public ActionResult Index(ViewModel model) {
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> GetResults(string searchQuery) {

            if (searchQuery.IsNullOrWhiteSpace()) {
                ViewBag.Error = "Var vänlig fyll i ett giltigt sökord.";
                return View("Index");
            }

            var searchQueryList = searchQuery.Split(' ');

            int? googleCount = 0;
            int? bingCount = 0;
            foreach (var queryWord in searchQueryList) {
                googleCount += await GetGoogleResultCountAsync(queryWord);
                bingCount += await GetBingResultCountAsync(queryWord);
            }

            return RedirectToAction("Index", "Home", new ViewModel {
                GoogleSearchHits = googleCount != null ? FormatNumberToString((int)googleCount) : null, 
                BingSearchHits = bingCount != null ? FormatNumberToString((int)bingCount) : null
            });
        }

        private static async Task<int?> GetGoogleResultCountAsync(string query) {
            try {
                var apiKey = "AIzaSyDMubsBo9U8DD8d0BeW7TkKQdB3zCC5ow0";
                var searchEngineId = "ff7becee8c7b88cc0";

                var svc = new Google.Apis.Customsearch.v1.CustomsearchService(
                    new BaseClientService.Initializer { ApiKey = apiKey });
                var listRequest = svc.Cse.List();

                listRequest.Cx = searchEngineId;
                listRequest.Q = query;
                var search = await listRequest.ExecuteAsync();
                var resultCount = search?.SearchInformation?.FormattedTotalResults ?? "0";
                var parseSuccess = int.TryParse(Regex.Replace(resultCount, "[^0-9.]", string.Empty), out int result);

                if (parseSuccess) {
                    return result;
                }

                return null;
            }
            catch {
                return null;
            }
        }

        private static async Task<int?> GetBingResultCountAsync(string query) {
            try {
                var doc = await new HtmlWeb().LoadFromWebAsync($"http://www.bing.com/search?q=" + query);
                var node = doc.DocumentNode.SelectNodes("//span[@class='sb_count']");
                
                var resultCount = node?.FirstOrDefault()?.InnerHtml ?? "0";
                var decodedResult = HttpUtility.HtmlDecode(resultCount);

                return int.Parse(Regex.Replace(decodedResult, "[^0-9.]", string.Empty));
            }
            catch {
                return null;
            }
        }

        private static string FormatNumberToString(int numberResult) {
            return numberResult.ToString("N0", CultureInfo.InvariantCulture);
        }
    }
}